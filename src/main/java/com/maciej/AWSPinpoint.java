package com.maciej;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.pinpoint.AmazonPinpointClientBuilder;

public class AWSPinpoint {

	public AWSPinpoint() {
		
	}

	public boolean testBuildClient() {
		try {
			System.out.println("a");
			AmazonPinpointClientBuilder clientBuilder = AmazonPinpointClientBuilder.standard().withRegion(Regions.US_WEST_2);
			System.out.println("b");
			clientBuilder.build();
			System.out.println("c");
		} catch(Exception e) {
			System.out.println("something bad happened. Exception: " + e.getMessage());
		}
		return true;
	}
	
}
